import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VochanComponent } from './main/vochan.component';

const routes: Routes = [
    {
        path: '',
        component:  VochanComponent,
    },
    {
        path: '**',
        redirectTo: ''
    }
];
@NgModule({
    imports:    [
        RouterModule.forRoot (routes)
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
