import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { VochanComponent } from './main/vochan.component';

import { AlertComponent, ConfirmationComponent, LoadingComponent } from './_lib_component';

import { MessageService } from './message/message.service';
import {
   WebSocketService,
   WebRTCService, RecorderService,
   SDPService,
   SupportService,
   PearService,
   AudioService
} from './service';

import { SubjectsService } from './service';

import {
  UserService, ContentService
} from './service';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    ConfirmationComponent,
    LoadingComponent,
    VochanComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    SubjectsService,
    MessageService,
    WebSocketService,
    WebRTCService, RecorderService, SDPService,
    SupportService, PearService,
    UserService, ContentService,
    AudioService
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
